package com.company;

public class ProcessorArm extends Processor{
    public static final String ARCHITECTURE = "ARM";

    public ProcessorArm(long frequency, long cache, long bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    public String dataProcess(String data) {
        if(data == null){
            return null;
        }
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {

        return String.valueOf(data / 50);
    }

    @Override
    public String toString() {
        return "Processor{ architecture= " + ARCHITECTURE +
                ", frequency=" + getFrequency() +
                ", cache=" + getCache() +
                ", bitCapacity=" + getBitCapacity() +
                '}';
    }
}
