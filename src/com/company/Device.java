package com.company;
import java.util.ArrayList;

public class Device {
    private Processor processor;
    private Memory memory;

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    public void save (String[] data){
        for (int i = 0; i < data.length; i++) {
            if(!memory.save(data[i])){
                System.out.println("Недостаточно памяти");
                return;
            }
        }
    }

    public String[] readAll(){
        ArrayList<String> buffer = new ArrayList<>();
        String bufferValue;

        while(true){
            bufferValue = memory.removeLast();
            if(bufferValue != null){
                buffer.add(0, bufferValue);
            } else {
                break;
            }
        }
        String[] result = new String[buffer.size()];
        buffer.toArray(result);
        return result;
    }


    public void dataProcessing(){
        String[] buffer = readAll();
        for(int i = 0; i < buffer.length; i++){
            buffer[i] = processor.dataProcess(buffer[i]);
        }
        save(buffer);
    }

    public String getSystemInfo(){
        return processor.getDetails() + "\n" + memory.getMemoryInfo().toString();
    }
}

