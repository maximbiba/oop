package com.company;

public abstract class Processor {
    private long frequency;
    private long cache;
    private long bitCapacity;

    public long getFrequency() {
        return frequency;
    }

    public long getCache() {
        return cache;
    }

    public long getBitCapacity() {
        return bitCapacity;
    }

    public String getDetails(){
        return toString();
    }

    public Processor(long frequency, long cache, long bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    abstract String dataProcess(String data);

    abstract String dataProcess(long data);

}

